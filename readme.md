1. Set `token` and `apiAddress` variables in `deepgram_example.js`
2. Run `npm i` 
3. Run `npm start`

In order to test a file generated from the platform change `400ms.wav` in line 37 to `halo.wav` - it's a recording that takes over 7 seconds, API seems to respond once every 16 KB sent.
