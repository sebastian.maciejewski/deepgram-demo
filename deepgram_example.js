const WebSocket = require('ws');
const qs = require('querystring');
const _ = require('lodash');
let fs = require('fs');
// Include the real-time streaming endpoint for the Deepgram API.

const token = ``; // Replace with token
const apiAddress = `` // Replace with API address
let bytesSent = 0;

if(!token) {
  console.log('Fill token variable in deepgram_example.js');
  process.exit(1);
}

const ws = new WebSocket(`${apiAddress}/v2/listen/stream?${qs.encode({
  model: 'general-dQw4w9WgXcQ',
  version: 'latest',
  language: 'en-US',
  punctuate: false,
  interim_results: true,
  encoding: 'linear16',
  channels: 1,
  sample_rate: 8000,
  endpointing: false,
})}`, {
  headers: {
    Authorization: token,
  },
});

ws.on('open', function open() {
  console.log('Will send audio file');
  // Audio file to stream. Make sure it is in the same directory as this script.
  // highWaterMark is set to 4KB in order to emulate the buffer size that we receive from our voice source (Asterisk)

  reader = fs.createReadStream('400ms.wav', { highWaterMark: 4096 });
  // reader = fs.createReadStream('halo.wav', { highWaterMark: 4096 });
  
  console.time('answer')
  console.time('sending')

  sendData(reader);
});

ws.on('message', function incoming(data) {
  const recognitionData = JSON.parse(data);
  console.timeLog('answer');
  //  console.log(recognitionData.duration);
});


function sendData(reader) {
  let data = reader.read()
  if (data) {
    bytesSent += data.length;
    ws.send(data)
    // Here we observe that Deepgram API responds only after receiving 4 * 4KB of audio data
    console.timeLog('sending', data.length, ` overall, ${bytesSent} bytes sent (${bytesSent/4096} * 4096)`)
  }
  setTimeout(() => sendData(reader), 200)
}